<?php
defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2013112502; // The current module version (Date: YYYYMMDDXX)
$plugin->release   = "1.9.0 RC3";
$plugin->requires  = 2012120300; // Requires this Moodle version (2.4)
$plugin->component = 'theme_decaf';
$plugin->maturity  = MATURITY_RC;