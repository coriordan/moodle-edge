Quick'ish Install for Moodle Edge using XAMPP for Mac OSX
=========================================================

- Download the XAMPP application for Mac OSX from https://www.apachefriends.org/
- Complete the installation of XAMPP choosing the default options.
- From the Terminal type: 
          
~~~
cd /Applications/XAMPP/xamppfiles/htdocs
git clone https://edel020@bitbucket.org/coriordan/moodle-edge.git
~~~

- You should now have a newly created Moodle application directory inside your
   XAMPP application install.

- From the same Terminal window type:

~~~
cd /Applications/XAMPP/xamppfiles/htdocs
mkdir moodle-edge_data
chmod 777 moodle-edge_data  
~~~
 
- Locate 'config.php' in the Moodle application directory (/Applications/XAMPP/xamppfiles/htdocs/moodle-edge).
- Edit the following entries:

~~~
$CFG->wwwroot = "http://localhost/moodle-edge";   
$CFG->dataroot  = "/Applications/XAMPP/xamppfiles/htdocs/moodle-edge_data";  
~~~

- Browse to http://localhost/phpmyadmin/ (if you opted to run Apache from a port other than port 80,  include the port number in the url).
- Create a new database called 'moodle_edge'. Select 'utf8_general_ci' as the database collation type.
- In the 'Databases' tab, click on 'Check Privileges' next to the 'moodle_edge' database you just created.
- Click on 'Add user' and add the following entries:

~~~
User name - moodle_edge
Host      - Local (localhost)
Password  - see config.php for password (or use your own. You will have to update config.php to reflect this!)
Re-type   - re-enter password
~~~

- Click 'Go' in the bottom right hand region of the screen (tiny button, very easy to miss!!)
- Download the Moodle Edge developer database from Dropbox using the following link:
    - <https://www.dropbox.com/s/xuoouqa0qgd06s1/moodle_edge_dev.sql>
- Select the 'Import' tab and under the 'File to Import' section, click on 'Choose File' and browse to the Moodle Edge developer database you just downloaded.
- Once import has been completed, you will have a fully populated Moodle Edge developer database.
- Browse to  <http://localhost/moodle-edge> to access your new Moodle Edge site. 
- To login as a student, use the following login:

~~~   
username: student
password: Student123
~~~    

- To login as a lecturer, use the following login:

~~~    
username: lecturer
password: Lecturer123
~~~

To login as admin, use the following login:

~~~ 
username: admin
password: Moodle-edge123
~~~

- The development database has been intentionally created without any included courses. 2 pre-designed Moodle courses can be downloaded and restored into your development version of Moodle Edge from the following Dropbox links:
    
    -[Application Development & Modelling](https://www.dropbox.com/s/2vg75i4k3ptlisj/backup-moodle2-course-14-app-dev-20140415-1111.mbz)    
    -[Design Patterns](https://www.dropbox.com/s/y4f834bud40v3uf/backup-moodle2-course-5-patterns-20140415-1234-nu.mbz)

TODO - Instructions on importing these courses.
