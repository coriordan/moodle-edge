
COURSE FORMATS menutopic
============================

Package tested in: moodle 2.3+

QUICK INSTALL
==============
Download zip package, extract the menutopic folder and upload this folder into course/format/.

ABOUT
=============
Developed by: David Herney Bernal Garcia - davidherney at gmail dot com
Information in: http://aprendeenlinea.udea.edu.co/lms/investigacion/course/view.php?id=50&topic=4

IN VERSION
=============

2013200101:
- Compatibility with moodle 2.4.
- Thanks to Jose Meza Hernandez. Your donation helped to update this version.

2012090104: Created!!!
- Menu Tree Construction
- Control of navigation between sections
- CSS Template
- JavaScript template
